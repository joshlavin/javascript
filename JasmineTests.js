describe("App", function() {
    'use strict';

    var path = '';
    if (typeof window.__karma__ !== 'undefined') {
      path += 'base/test/'
    }
    jasmine.getFixtures().fixturesPath = path + 'fixtures';

    beforeEach(function() {
        loadFixtures('app.html');
    });

    describe("functions", function() {
        it("should add options and select default", function() {
            var assignedObj = {
                '123': "User, Mister",
                '456': "User, Misses"
            };
            App.addOptions( 'jselect', assignedObj, '456');
            expect($("#jselect > option")).toHaveLength(3);
            expect($("#jselect option[value=456]")).toBeSelected();
        });
        it("should set up key:value object from array of objects", function() {
            var aryObj = [
                { version: "bar", page: '143' },
                { version: "baz", page: '533' }
            ];
            var newObj = App.jsonToKeyValue(aryObj,'version','page');
            expect(newObj.bar).toEqual('143');
        });
        it("should create organized divs in 2 columns", function() {
            var result = App.divOrganize( $("#organize > div"), 2, 'myclass');
            expect(result).toHaveLength(2);  // two of div.row
            expect(result[0]).toHaveClass('row');
            expect( result[0].children[0] ).toHaveClass('myclass');
        });
    });

    describe("validation", function() {
        it("should fail if required fields don't have a value", function() {
            var reqField = [{
                id: "myinput"
            }];
            expect(App.validateFields(reqField)).toBeFalsy();
        });
        it("should fail if uploading a BMP file to an image field", function() {
            var reqImg = [{
                    id: "myimage",
                    check: "web_img"
            }];
            expect(App.validateFields(reqImg)).toBeFalsy();
        });
    });

    describe("set up (jQuery)", function() {
        beforeEach(function() {
          App.onReady();
        });
        it("should add navigation tabs", function() {
          expect($("#tabs")).toExist();
          expect($("#tabs")).toContainElement('li');
        });
        it("should load comments", function() {
          expect($("#comments")).toExist();
          expect($("#comments > div:visible > .cmt_name:first")).toHaveText('josh');
        });
        it("should add answer choice textareas", function() {
          expect($("#choice")).toExist();
          expect($("#choice_a")).toExist();
          expect($("#choice_a")).toHaveText('Choice A');
        });
    });

});
