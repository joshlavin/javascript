var App = (function() {
	'use strict';

	var selectsKeyVal = {};   // store key-value objects here, for later use

	var requiredFields = [
		{
			id: "question"
		},
		{
			id: "choice_a"
		},
		{
			id: "image_top",
			check: 'web_img',
			alert: 'Only PNG, JPG, or GIF files are accepted.'
		}
	];

	var selects = [
		{
			element: "assigned_to",
			origObj: data.users,
			keyStr:  'username',
			valStr:  'name',
			defOpt:  q_assigned_to
		},
		{
			element: "status",
			origObj: data.statuses,
			keyStr:  0,
			valStr:  1,
			defOpt:  q_status
		},
		{
			element: "category",
			origObj: data.categories,
			keyStr:  'code',
			valStr:  'category',
			defOpt:  q_category
		}
	];

	//
	// optionObj has the format: { value: text, value: text }
	//
	var addOptions = function (targId, optionObj, defaultOpt) {
		var targ = document.getElementById(targId);
		if (!targ) return false;
		for (var i in optionObj) {
			var selected = (i == defaultOpt) ? 1 : 0;
			targ.options.add( new Option(optionObj[i], i, selected) );
		}
	}

	//
	// takes something like: [ { key: 'foo', val: 'bar' } ]
	// or: array: [ ['foo','bar'], ['baz','buz'] ]
	// and turns into: object: { foo: 'bar' }
	//
	var jsonToKeyValue = function (obj, key, val) {
		var newObj = {};
		for (var i = 0; i < obj.length; i++) {
			newObj[ obj[i][key] ] = obj[i][val];
		}
		return newObj;
	}

	var _vanillaRemoveClass = function (e,c) {
		e.className = e.className.replace( new RegExp('(?:^|\\s)'+c+'(?!\\S)') ,'');
	}

	var divOrganize = function (el, cols, div_class) {
		cols = parseInt(cols) || 2;
		for (var e in el) {
			el[e].className = el[e].className + ' ' + div_class;
		}
		var org = [];
		while (el.length > 0) {
			var row = document.createElement('div');
			row.className = 'row';
			var spl = el.splice(0, cols);
			for (var i = 0; i < spl.length; i++) {
				row.appendChild(spl[i]);
			}
			org.push(row);
		}
		return org;
	}

	var validateFields = function (reqObj) {
		var ok = true;
		var reqObjLength = reqObj.length;
		for (var i = 0; i < reqObjLength; i++) {
			var el = document.getElementById(reqObj[i].id);
			if (!el) continue;
			var twoParentsUp = el.parentNode.parentNode;
			_vanillaRemoveClass(twoParentsUp,'has-error');
			var elok = true;
			if (reqObj[i].check == 'web_img') {
				if (el.value && !/\.(?:png|jpe?g|gif)$/i.test(el.value)) elok = false;
			}
			else {
				// default check is for a value to be present
				if (!el.value) elok = false;
			}
			if (!elok) {
				ok = false;
				twoParentsUp.className = twoParentsUp.className + ' has-error';
				if (reqObj[i].alert) alert(reqObj[i].alert);
			}
		}
		return ok;
	}

	var _view = function () {
		var selectsLength = selects.length;
		for (var i = 0; i < selectsLength; i++) {
			selectsKeyVal[ selects[i].element ] = jsonToKeyValue(selects[i].origObj, selects[i].keyStr, selects[i].valStr);
			addOptions(selects[i].element, selectsKeyVal[ selects[i].element ], selects[i].defOpt);
		}
	}();

	var _collapseShowHide = function(event) {
		var remCl = event.data.vis == 'show' ? 'small' : 'full'
		var addCl = event.data.vis == 'show' ? 'full' : 'small';
		$("#" + event.target.id ).prev().find("span").removeClass('glyphicon-resize-'+remCl).addClass('glyphicon-resize-'+addCl);
	}

	var onReady = function() {

		//
		// Initial setup of form elements, not already done above.
		//

		// menu and first tab
		var tabFrag = document.createDocumentFragment();
		var menuLength = data.menu.length;
		for (var i = 0; i < menuLength; i++) {
			var $tmp = $("<li></li>").html('<a role="tab" data-toggle="tab"></a>');
			$tmp.children().attr("href", "#"+ data.menu[i][0] ).text( data.menu[i][1] );
			if (i == 0) {
				$tmp.addClass("active");
				$("#" + data.menu[i][0] ).addClass("in active").show();  // tab content
			}
			tabFrag.appendChild($tmp[0]);
		}
		$("#tabs")[0].appendChild(tabFrag);

		// comments
		var $comments = $("#comments");
		var $cmtTpl = $comments.children();
		var cmtFrag = document.createDocumentFragment();
		var cmtLength = data.comments.length;
		for (var i = 0; i < cmtLength; i++) {
			var tmp = $cmtTpl.clone();
			tmp.show()
				.find(".cmt_name").text( data.comments[i].name ).end()
				.find(".cmt_date").text( data.comments[i].pretty_date ).end()
				.find(".cmt_comment").text( data.comments[i].comment ).end()
				;
			cmtFrag.appendChild(tmp[0]);
		}
		$comments[0].appendChild(cmtFrag);
		if ( cmtLength == 0 ) {
			$comments.text("None.");
		}

		// answers (choices)
		var $choice = $("#choice");
		var choiceFrag = document.createDocumentFragment();
		for (var i in App.selectsKeyVal.answers) {
			var choiceName = "choice_" + i.toLowerCase();
			var $tmp = $choice.clone().show().removeAttr('id');
			$tmp.find('label').attr("for",choiceName).text(function(ix,t) {
					return t.replace( /X$/, i );
				}).end()
				.find('textarea').attr({ name: choiceName, id: choiceName }).text(choices[i])
				;
			choiceFrag.appendChild($tmp[0]);
		}
		$choice.before(choiceFrag);

		//
		// Form validation.
		//

		$("#save-edit").click(function() {
			if (validateFields(requiredFields)) {
				$(this).val('saving...');
			}
			else {
				alert('Please correct fields in red.');
				return false;
			}
		});

		$("#save-addl").click(function() {
			// none required.
			$(this).val('saving...');
		});

		//
		// Others.
		//

		$("#cmtref").on('show.bs.collapse', { vis: 'show' }, _collapseShowHide);
		$("#cmtref").on('hide.bs.collapse', { vis: 'hide' }, _collapseShowHide);

	} //end onReady

	return {
		selectsKeyVal: selectsKeyVal,
		addOptions: addOptions,
		jsonToKeyValue: jsonToKeyValue,
		divOrganize: divOrganize,
		validateFields: validateFields,
		onReady: onReady
	};

})();
